## Project Instructions 🔖

Storybook 🔥 ✅

## Getting started 🔨

- Installere dependencies (yarn)
- Kjør prosjektet (yarn storybook)

### Development Environment and Architecture 🧰

- TS
- CSS

### Session

#### 1. How we use CSS in JS (Storybook) 💅 ✅

- Styled Components
- Lag en Button komponent fra Scratch
  - Vis komponenten fra Design Systemet
  - Lag komponenten
  - Lag stories
- Gjennomgang av mt-ui
  - Grid and Friends
  - TextArea
  - TextLink
  - Header
  - Text

##### Learn More 📚

- Storybook https://storybook.js.org/
- Styled Components https://styled-components.com/
