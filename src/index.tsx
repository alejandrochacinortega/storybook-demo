import React from 'react'
import ReactDOM from 'react-dom'
import Button from './components/Button'
import Header from './components/Header'

const App = () => (
  <div>
    {/* <Button size="medium">Hello medium</Button>
    <Button size="tiny">Hello tiny</Button>
    <Button size="applicationHeader">Hello application header</Button> */}
    <Header>Hello</Header>
    <Header size="heading1">Hello</Header>
    <Header size="heading2">Hello</Header>
  </div>
)

// TODO:
ReactDOM.render(<App></App>, document.querySelector('#root'))
