import { blue, green, neutral, red, yellow } from './colors'

export const defaultTheme = {
  primaryBackgroundColor: '#93221C',
  primaryTextColor: '#fff',
  secondaryBackgroundColor: '#fff',
  secondaryTextColor: '#93221C',
  // primaryColor: blue[300],
  primaryHoverColor: blue[200],
  primaryActiveColor: blue[100],
  textColorOnPrimary: neutral[100],
  textColor: neutral[600],
  textColorInverted: neutral[100],
  disabled: '#ECECEC',
  textOnDisabled: '#FFF',
  activeBackground: '#676560',
  activeText: '#FFF',
  status: {
    warningColor: yellow[100],
    warningColorHover: yellow[200],
    warningColorActive: yellow[300],
    errorColor: red[100],
    errorColorHover: red[200],
    errorColorActive: red[300],
    successColor: green[100],
    successColorHover: green[200],
    successColorActive: green[300],
  },
}
