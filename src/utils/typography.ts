/**
 * Sizes supported (px).
 * tiny 11, small 12, medium 14, normal 16, heading3 18, heading2 20, heading1 24, applicationHeader 36
 */
export const size = {
  tiny: '0.6875rem', // 11px
  small: '0.75rem', // 12px
  medium: '0.875rem', // 14px
  normal: '1rem', // 16px
  heading3: '1.125rem', // 18px
  heading2: '1.25rem', // 20px
  heading1: '1.5rem', // 24px
  applicationHeader: '2.25rem', // 36px
}

/**
 * Sizes supported for button (px).
 * small 12, medium 14, normal 16,
 */
export const buttonSize = {
  small: '12px', // 12px
  medium: '14px', // 14px
  large: '18px', // 18px
}

/**
 *  bold 700, semibold 600, regular 400, light 300, lighter
 */
export const weight = {
  bold: '700',
  semibold: '600',
  regular: '400',
  light: '300',
  lighter: 'lighter',
}

/**
 *  normal and italic
 */
export const fontStyle = {
  normal: 'normal',
  italic: 'italic',
}
