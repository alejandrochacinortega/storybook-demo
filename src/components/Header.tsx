import classNames from 'classnames'
import React, { ReactNodeArray, ReactNode } from 'react'
import styled from 'styled-components'
import * as Typography from '../utils/typography'

export interface IHeader {
  /**
   *  HTML header elements
   */
  as?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
  /**
   *  Desired info goes here
   */
  children: ReactNodeArray | ReactNode
  /**
   * Sizes supported (px).
   * tiny 11, small 12, medium 14, normal 16, heading3 18, heading2 20, heading1 24, applicationHeader 36
   */
  size?: keyof typeof Typography.size
  /**
   *  bold 700, semibold 600, regular 400
   */
  weight?: keyof typeof Typography.weight
  /**
   *  Make text uppercase
   */
  uppercase?: boolean
  /**
   *  Make text lowercase
   */
  lowercase?: boolean
  /**
   *  Make text capitalize
   */
  capitalize?: boolean
  /**
   *  Center header
   */
  center?: boolean
  /**
   * Custom class
   */
  className?: string
}

const StyledHeader = styled.div<IHeader>`
  font-size: ${({ size }) => Typography.size[size || 'normal']};
  font-weight: ${({ weight }) => Typography.weight[weight || 'bold']};

  &.uppercase {
    text-transform: uppercase;
  }

  &.capitalize {
    text-transform: capitalize;
  }

  &.lowercase {
    text-transform: lowercase;
  }

  &.center {
    text-align: center;
  }
`

const Header = ({
  as,
  children,
  size,
  weight,
  uppercase,
  lowercase,
  capitalize,
  center,
  className,
}: IHeader) => {
  const headerClasses = classNames('Header', className, {
    uppercase,
    lowercase,
    capitalize,
    center,
  })

  return (
    <StyledHeader className={headerClasses} as={as} size={size} weight={weight}>
      {children}
    </StyledHeader>
  )
}

export default Header
