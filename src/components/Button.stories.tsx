import React from 'react'

import { Story, Meta } from '@storybook/react'
import Button, { IButton } from './Button'

const Template: Story<IButton> = (args) => <Button {...args} />

export const Primary = Template.bind({})
Primary.args = {
  mode: 'primary',
}

export const Secondary = Template.bind({})
Secondary.args = {
  mode: 'secondary',
}

export const Large = Template.bind({})
Large.args = {
  size: 'large',
}

export const Medium = Template.bind({})
Medium.args = {
  size: 'medium',
}

export const Small = Template.bind({})
Small.args = {
  size: 'small',
}

export const Disabled = Template.bind({})
Disabled.args = {
  disabled: true,
}

export const LargePrimary = Template.bind({})
LargePrimary.args = {
  mode: 'primary',
  size: 'large',
}

export default {
  title: 'Components/Button',
  component: Button,
  args: {
    children: 'Button Text',
  },
} as Meta
