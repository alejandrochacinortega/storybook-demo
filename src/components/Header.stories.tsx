import React from 'react'
import Header, { IHeader } from './Header'
import { Story, Meta } from '@storybook/react/types-6-0'
import './component.css'

export default {
  title: 'Components/Header',
  component: Header,
  args: {
    as: 'h1',
    size: 'huge',
    weight: 'bold',
    children: 'Heading default values',
  },
} as Meta

const Template: Story<IHeader> = (args) => <Header {...args} />

export const HeaderApplication = Template.bind({})
HeaderApplication.args = {
  size: 'applicationHeader',
  children: 'Aplication Heading 36 Bold',
}

export const Header1Bold = Template.bind({})
Header1Bold.args = {
  size: 'heading1',
  children: 'H1 Heading 24 Bold',
}

export const Header2Bold = Template.bind({})
Header2Bold.args = {
  as: 'h2',
  size: 'heading2',
  children: 'H2 Heading 20 Bold',
}

export const Header3Bold = Template.bind({})
Header3Bold.args = {
  as: 'h3',
  size: 'heading3',
  children: 'H3 Heading 18 Bold',
}

export const Header4Bold = Template.bind({})
Header4Bold.args = {
  as: 'h4',
  size: 'normal',
  children: 'H4 Heading 16 Bold',
}

export const Header5SemiBold = Template.bind({})
Header5SemiBold.args = {
  as: 'h5',
  size: 'normal',
  weight: 'semibold',
  children: 'H5 Heading 16 semibold',
}

export const Header5Small = Template.bind({})
Header5Small.args = {
  as: 'h5',
  size: 'medium',
  weight: 'semibold',
  children: 'H5 Heading 14 medium',
}

export const Header5Uppercase = Template.bind({})
Header5Uppercase.args = {
  as: 'h5',
  size: 'normal',
  children: 'H5 Heading 16 uppercase',
  uppercase: true,
}

export const Header5Capitalize = Template.bind({})
Header5Capitalize.args = {
  as: 'h5',
  size: 'normal',
  children: 'H5 Heading 16 capitalize',
  capitalize: true,
}

export const Header5Lowercase = Template.bind({})
Header5Lowercase.args = {
  as: 'h5',
  size: 'normal',
  children: 'H5 Heading 16 lowercase',
  lowercase: true,
}

export const Header5Center = Template.bind({})
Header5Center.args = {
  as: 'h5',
  size: 'normal',
  children: 'H5 Heading 16 center',
  center: true,
}

export const HeaderWithCustomClass = Template.bind({})
HeaderWithCustomClass.args = {
  size: 'normal',
  weight: 'bold',
  children: 'Large 16 bold',
  className: 'header-custom-class',
}
