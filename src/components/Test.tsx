import classNames from 'classnames'
import React, { ReactNode, ReactNodeArray } from 'react'
import styled from 'styled-components'
import * as Utilities from '../utils/typography'

export interface IButton {
  /**
   *  Desired info goes here
   */
  children: ReactNodeArray | ReactNode
  /**
   *  Make text capitalize
   */
  capitalize?: boolean
  /**
   * Sizes supported (px).
   * tiny 11, small 12, medium 14, normal 16, heading3 18, heading2 20, heading1 24, applicationHeader 36
   */
  size?: keyof typeof Utilities.size
  /**
   * Custom class
   */
  className?: string
}

const StyledButton = styled.button<IButton>`
  background-color: red;
  border: none;
  color: white;
  padding: 12px 24px;

  &.size {
    font-size: ${(props) => Utilities.size[props.size || 'medium']};
  }

  &.capitalize {
    text-transform: uppercase;
  }
`

const Button = ({ children, className, capitalize, size }: IButton) => {
  const buttonClasses = classNames('Button', className, {
    capitalize,
    className,
    size,
  })

  return (
    <StyledButton className={buttonClasses} size={size}>
      {children}
    </StyledButton>
  )
}

// export default PrimaryButton
export default Button
