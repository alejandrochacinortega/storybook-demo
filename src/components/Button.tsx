import classNames from 'classnames'
import React, { ReactNode, ReactNodeArray } from 'react'
import styled from 'styled-components'
import { defaultTheme } from '../utils/themes'
import * as Utilities from '../utils/typography'

interface IButtonBase {
  disabled?: boolean
}

export interface IButton extends IButtonBase {
  children: ReactNodeArray | ReactNode
  size: 'small' | 'medium' | 'large'
  mode: 'primary' | 'secondary'
}

interface IButtonStyled extends IButtonBase {
  primary?: boolean
  secondary?: boolean
  large?: boolean
  medium?: boolean
  small?: boolean
}

const StyledButton = styled.button<IButtonStyled>`
  &.Button {
    border-radius: 21px;
    background-color: #93221c;
    padding: 12px 20px;
    font-size: ${Utilities.buttonSize.large};
    font-weight: bold;
    letter-spacing: 0;
    line-height: 20px;
    text-align: center;
    color: white;
  }

  &.primary {
    background-color: ${defaultTheme.primaryBackgroundColor};
    color: ${defaultTheme.primaryTextColor};
    border-color: none;
  }

  &.secondary {
    background-color: ${defaultTheme.secondaryBackgroundColor};
    color: ${defaultTheme.secondaryTextColor};
    border-color: ${defaultTheme.primaryBackgroundColor};
  }

  &.large {
    padding: 12px 20px;
    font-size: ${Utilities.buttonSize.large};
  }

  &.medium {
    padding: 11px 16px;
    font-size: ${Utilities.buttonSize.medium};
  }

  &.small {
    padding: 7.5px 12px;
    font-size: ${Utilities.buttonSize.small};
  }

  &:hover {
    background-color: ${defaultTheme.activeBackground};
    color: ${defaultTheme.activeText};
    border-color: none;
  }

  &.disabled {
    background-color: ${defaultTheme.disabled};
    color: white;
    border-color: none;
    border: 2px solid transparent;
    cursor: not-allowed;
  }
`

const Button = ({ children, disabled, mode, size }: IButton) => {
  const buttonClasses = classNames('Button', {
    disabled,
    large: size === 'large',
    medium: size === 'medium',
    small: size === 'small',
    primary: mode === 'primary',
    secondary: mode === 'secondary',
  })
  return <StyledButton className={buttonClasses}>{children}</StyledButton>
}

export default Button
